import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

abstract class BoardObject {
    protected int x,y;
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public BoardObject(int x, int y) {
        this.x = x;
        this.y = y;
    }
        
    public void setPos(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public double distance(BoardObject e){
        return sqrt(pow(e.x-x,2) + pow(e.y-y,2));
    }
    
    Place localization() {
        return new Place(x,y);
    }
    @Override
    public String toString(){
        return "(" + Integer.toString(x) + ", " + Integer.toString(y) + ")";
    }
}

class Place extends BoardObject{
    public Place(int x, int y) {
        super(x, y);
    }
}

abstract class MovingEntity extends BoardObject {
    
    private int strength;
    private int id;
    
    public MovingEntity(int id, int x, int y, int strength){
        super(x,y);
        this.strength = strength;
        this.id = id;
    }

    public void setStrength(int s){
        this.strength = s;
    }
    
    public int getStrength(){
        return this.strength;
    }
    
    public int getId(){
        return id;
    }
    
}

