class Ghost extends MovingEntity implements Comparable {

    private int busters;

    Ghost(int id, int x, int y, int stamina, int busters) {
        super(id, x, y, stamina);
        this.busters = busters;
    }

    boolean busted() {
        return getStrength()==-1;
    }
    
    void setBusted(){
        setStrength(-1);
    }

    int getStamina(){
        return getStrength();
    }
    
    boolean valid() {
        return !busted() && (x!=0 || y!=0);
    }

    @Override
    public int compareTo(Object t) {
        if (!(t instanceof Ghost)) {
            throw new ClassCastException("A Ghost object expected.");
        }
        return ((Ghost) t).getStamina() - getStamina();
    }

    double getBusters() {
        return busters;
    }
}

class DumbGhost extends Ghost{
    public DumbGhost() {
        super(-1,-1,-1, Integer.MAX_VALUE, 0);
    }
    @Override
    boolean valid() {
        return false;
    }
    @Override
    public int getStamina(){
        return Integer.MAX_VALUE;
    }
    @Override
    public int compareTo(Object t){
        return -1;
    }
}