
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

class Game {
    private final int xdim = 16000;
    private final int ydim = 9000;
    private final int range = 2200;
    private final Random rnd = new Random();
    private int ghostCount = 0;
    private final Buster[] busters;
    protected Map<Integer, Buster> opponents;
    protected Map<Integer, Ghost> ghosts;
    protected Map<Integer, Ghost> coolGhosts;
    protected int helpNeeded;
    public Place base;
    public Place opBase;
    private int round = 0;
    private List<Place> destinations;
    protected List<Integer> toStun;
    protected int lastRadar;
    
    public Game(int ghostCount, int busterCount, int playerId) {
        this.ghostCount = ghostCount;
        busters = new Buster[busterCount];
        toStun = new LinkedList<>();
        /*Bases should be set up before buster configuring */
        if(playerId==0){
            base = new Place(0, 0);
            opBase = new Place(xdim-1, ydim-1);
        }else{
            opBase = new Place(0, 0);
            base = new Place(xdim-1, ydim-1);
        }
        
        for(int i = 0; i< busterCount; i++){
            busters[i] = new Buster(i,0,0,0, this);
        }
        coolGhosts = new HashMap<>();
        ghosts = new HashMap<>();
        opponents = new HashMap<>();
        destinations = new LinkedList<>();
        for(int x = 0; x<=xdim; x+=2000){
            destinations.add(new Place(x,0));
            destinations.add(new Place(x,ydim));
        }
        for(int y = 1500; y<ydim; y+=1500){
            destinations.add(new Place(0,y));
            destinations.add(new Place(xdim,y));
        }
        Collections.shuffle(destinations);
        lastRadar = 0;
    }
    
    int getXdim(){
        return xdim;
    }
    
    int getYdim(){
        return ydim;
    }
    
    Ghost specialGhost(){
        return ghosts.get(helpNeeded);
    }
    
    void nextRound(){
        clear();
        round++;
    }
    
    int getRange(){
        return range;
    }
    
    void clear(){
        ghosts.clear();
        opponents.clear();
        toStun.clear();
    }

    void updateGhost(int id, int x, int y, int stamina, int busters) {
        Ghost g = new Ghost(id, x, y, stamina, busters);
        ghosts.put(id, g);
        if(stamina<16){
            /* Overwrites key if already defined */
            System.err.printf("Adding ghost %d\n", id);
            coolGhosts.put(id, g);
        }
    }

    void updateBuster(int id, int x, int y, int state, int ghostId) {
        busters[id].setPos(x, y);
        busters[id].setState(state, ghostId);
        int truncX = x - x%range;
        int truncY = y - y%range;
        Place close = null;
        for(Place p: destinations){
            if(p.distance(busters[id])<range){
                close = p;
                break;
            }
        }
        if(close!=null){
            destinations.remove(close);
            destinations.add(close);
        }
    }

    void updateOpponent(int id, int x, int y, int state, int value) {
        Buster op = new Buster(id, x, y, state, this);
        op.setState(state, value);
        opponents.put(id, op);
    }

    String busterAction(int id) {
        return busters[id].getAction();
    }

    boolean bustedGhost(Integer target) {
        Ghost g = ghosts.get(target);
        if(g == null)
            return true;
        else
            return g.busted();
    }

    int getRound() {
        return round;
    }

    Place getNewDestination() {
        
        Place d = destinations.get(0);
        destinations.remove(0);
        destinations.add(d);
        dbg("New place", d);
        return d;
    }
    
    void dbg(String s, Object o){
        System.err.println(s);
        System.err.println(o.toString());
    }
    
}
