
import static java.lang.Math.sqrt;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

enum State {
    Carrying,
    NotCarrying,
    Busting, 
    Stunned,
    Invalid
}

final class Buster extends MovingEntity {
    
    private final Random rnd;
    private final Game game;
    private Ghost ghost;
    private Integer target;
    private Place destination;
    
    private State state = State.NotCarrying;
    private boolean pingPong = true;
    private int lastStun = -20;
    private boolean radarNotUsed = true;
    
    public Buster(int id, int x, int y, int strength, Game game) {
        super(id, x, y, strength);
        this.game = game;
        rnd = new Random();
        destination = null;
    }
    
    State translate(int state){
        /* 0: idle or moving buster.
         * 1: buster carrying a ghost.
         * 2: stunned buster.
         * 3: buster in the process of trapping a ghost. */
        switch(state){
            case 0:  return State.NotCarrying;
            case 1:  return State.Carrying;
            case 2:  return State.Stunned;
            case 3:  return State.Busting;
            default: return State.Invalid;
        }
    }

    void setState(int newIntState, int ghostId) {
        State newState = translate(newIntState);
        
        if(newState != state){
            if(state == State.Busting){
                target = -1;
            }
            if(newState == State.Stunned ||
               newState == State.NotCarrying){
                ghost = null;
            }
            if(newState == State.Carrying){
                game.coolGhosts.remove(ghostId);
            }
            state = newState;
        }
    }

    String getAction() {
        System.err.printf("Buster %d: ", getId());
        System.err.println(state);
        /* If carrying a ghost - go to the base! */
        if(state == State.Carrying){
            if (distance(game.base)<1600){
                return release();
            }
            else{
                Buster opponent = opponentInRange();
                if(opponent != null && game.base.distance(opponent)<game.base.distance(this)-500){
                    return movePnd(opponent);
                }else
                    return moveTo(game.base);
            }
        }
        
        Ghost choice;
        List<Ghost> nearGhosts = ghostsInRange((int)(game.getRange()));
        if(!nearGhosts.isEmpty()){
            nearGhosts.sort((Ghost a, Ghost b) -> -(b.getStamina() - a.getStamina() + distanceBonus(b) - distanceBonus(a)));
            
            choice = nearGhosts.get(0);
            if(choice.getStamina()<(21-(game.getRound()-lastStun))){
                if(distance(choice)<900){
                    return moveAway(choice.x, choice.y);
                }
                if(distance(choice)>0.75*game.getRange()){
                    return moveTo(choice);
                }
                //nearGhosts.forEach((Ghost g) -> System.err.printf("Selecting gid %d, stamina %d, bonus %d\n", g.getId(), g.getStamina(), distanceBonus(g)));
                ghost = choice;
                target = choice.getId();
                return bust();
            }
        }
        
        for(Buster b:game.opponents.values()){
            //System.err.printf("op id %d (%d %d) %f %s\n", b.getId(), b.getX(), b.getY(), distance(b), b.state.toString());
            if(distance(b)<2400 && b.state == State.Carrying  && !game.toStun.contains(b.getId())){
                if(distance(b)<0.75*game.getRange() && (game.getRound()-lastStun>20)){
                    target = b.getId();
                    lastStun = game.getRound();
                    game.toStun.add(target);
                    return stun();
                }else{
                    System.err.printf("Try to stun %d\n", b.getId());
                    return moveTo(b);
                }
            }
        }
        
        
        /* Check if you can stun */
        Buster opponent = opponentInRange();
        if(opponent != null && (game.getRound()-lastStun>20) && !game.toStun.contains(opponent.getId())){
            lastStun = game.getRound();
            target = opponent.getId();
            game.toStun.add(target);
            return stun();
        }
        
        /* If busting - bust! */
        if(state == State.Busting){
            if(ghost.getBusters()>1){
                game.coolGhosts.put(target, ghost);
                game.helpNeeded = target;
            }
            return bust();
        }
        
        
        
        
        /* Check if you can trap a ghost */
        nearGhosts = ghostsInRange((int)(game.getRange()));
        if(!nearGhosts.isEmpty()){
            nearGhosts.sort((Ghost a, Ghost b) -> -(b.getStamina() - a.getStamina() + distanceBonus(b) - distanceBonus(a)));
        
            choice = nearGhosts.get(0);
            if(distance(choice)<900){
                return moveAway(choice.x, choice.y);
            }
            if(distance(choice)>0.75*game.getRange()){
                return moveTo(choice);
            }
            //nearGhosts.forEach((Ghost g) -> System.err.printf(" Selecting gid %d, stamina %d, bonus %d\n", g.getId(), g.getStamina(), distanceBonus(g)));
            ghost = choice;
            target = choice.getId();
            return bust();
        }else{
            /* Select something more distant. */
            if(game.helpNeeded != -1){
                Ghost g = game.ghosts.get(game.helpNeeded);
                if(g!=null){
                    System.err.print("Special help!\n");
                    return moveTo(g);
                }
            }
            if(game.getRound() - game.lastRadar>20 && radarNotUsed){
                game.lastRadar = game.getRound();
                radarNotUsed = false;
                return "RADAR";
            }
            /*
            if(game.specialGhost()!=null && (distance(game.specialGhost())<3000)){
                destination = game.specialGhost().localization();
                System.err.println("Special!");
            }*/
            if(!game.coolGhosts.isEmpty()){
                Ghost bestChoice = new DumbGhost();
                List<Integer> toRemove = new LinkedList<>();
                for(Ghost g: game.coolGhosts.values()){
                    if(distance(g)<game.getRange() && !nearGhosts.contains(g)){
                        System.err.printf("False alarm for %d!\n", g.getId());
                        toRemove.add(g.getId());
                    }
                    if(isBetter(bestChoice, g)){
                        bestChoice = g;
                    }
                }
                toRemove.forEach((Integer gid) ->  game.coolGhosts.remove(gid));
               
                if(game.ghosts.get(bestChoice.getId()) != null){
                    System.err.printf("Using cool list and gid %d\n", bestChoice.getId());
                    return moveTo(bestChoice);
                }
            }
             if(destination==null || distance(destination)<0.75*game.getRange()){
                 getNewDestination();
             }
             System.err.println("GoToDestination" + destination.toString());
            return moveTo(destination);
        }
    }
    
    void getNewDestination(){
        destination = game.getNewDestination();
        /*if(destination == null){
            destination = new Place(0,0);
        }
        int tmp = rnd.nextInt(3);
        int xd,yd;
        if(pingPong == true){
            xd = game.opBase.getX();
            yd = game.opBase.getY();
        }else{
            xd = game.base.getX();
            yd = game.base.getY();
        }
        System.err.printf("newDest: %d %d -> %d %d %d\n", destination.getX(), destination.getY(), xd,yd,pingPong?1:0);
        pingPong = !pingPong;
        if(tmp==0){
            xd = game.getXdim() - xd;
        }else if(tmp==1){
            
        }else{
            yd = game.getYdim() - yd;
        }
        //if(tmp>16000) destination.setPos(xd, tmp-16000);
        destination.setPos(xd, yd);
        System.err.printf("after newDest: %d %d -> %d %d %d\n", destination.getX(), destination.getY(), xd,yd,pingPong?1:0);*/
    }
    
    private Buster carryingOpponent() {
        Buster ret = new Buster(-10000, -10000,-1, -1, this.game);
        for(Buster b:game.opponents.values()){
            if(b.state!=State.Stunned && (distance(b)<(0.75*game.getRange()))){
                ret = b;
            }
            if(ret.state==State.Carrying){
                return ret;
            }
        }
        if(ret.getId() == -1){
            return null;
        }else{
            return ret;
        }
    }
    
    /* Searching for nearest opponent */
    private Buster opponentInRange() {
        Buster ret = new Buster(-1, -10000, -10000, -1, this.game);
        for(Buster b:game.opponents.values()){
            if(b.state!=State.Stunned && (distance(b)<(0.75*game.getRange()))){
                ret = b;
            }
            if(ret.state==State.Carrying){
                return ret;
            }
        }
        if(ret.getId() == -1){
            return null;
        }else{
            return ret;
        }
    }
    
    private List<Ghost> ghostsInRange(int range) {
        List<Ghost> inRange = new LinkedList<>();
        for(Ghost g:game.ghosts.values()){
            if(distance(g)<range){
                inRange.add(g);
            }
        }
        return inRange;
    }
    
    /* Output string formatting */
    private String move(Integer destX, Integer destY){
        return "MOVE "+ destX.toString() + " " + destY.toString();    
    }
    
    private String moveAway(Integer destX, Integer destY){
        Integer nx = x - (destX - x)/2;
        Integer ny = y - (destY - y)/2;
        
        if((destX == x) && (destY == y)){
            nx = game.base.x;
            ny = game.base.y;
        }

        return "MOVE "+ nx.toString() + " " + ny.toString();    
    }
    
    private String bust(){
        return "BUST "+ target.toString();
    }
    
    private String release(){
        return "RELEASE";
    }

    private String stun(){
        return "STUN "+ target.toString();
    }

    private String moveTo(BoardObject b) {
        return move(b.getX(), b.getY());
    }

    
    private String moveAway(BoardObject b) {
        return moveAway(b.getX(), b.getY());
    }
    
    private String movePnd(BoardObject b) {
        double vx = b.getX() - x;
        double vy = b.getY() - y;
        double norm = sqrt(vx*vx + vy*vy);
        vx *= 800/norm;
        vy *= 800/norm;
        return moveAway(x-(int)vy, y-(int)vx);
    }

    private int distanceBonus(Ghost b) {
        if(distance(b)<0.75*game.getRange() && distance(b)>900){
            return -3;
        }else if(distance(b)<game.getRange()){
            return -1;
        }else{
            return 0;
        }
    }

    private boolean isBetter(Ghost bestChoice, Ghost g) {
        double distValue = distance(g) - distance(bestChoice);
        double bustersValue = bestChoice.getBusters() - g.getBusters();
        double staminaValue = g.getStamina() - bestChoice.getStamina();
        staminaValue = Math.pow(staminaValue, 3)*1000;
        bustersValue = Math.pow(bustersValue, 3)*1000;
        return (distValue + bustersValue + staminaValue)<0;
    }
}
