import java.util.Scanner;

class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int bustersPerPlayer = in.nextInt(); // the amount of busters you control
        int ghostCount = in.nextInt(); // the amount of ghosts on the map
        int myTeamId = in.nextInt(); // if this is 0, your base is on the top left of the map, if it is one, on the bottom right
        Game game = new Game(ghostCount, bustersPerPlayer, myTeamId);
        // game loop
        while (true) {
            int entities = in.nextInt(); // the number of busters and ghosts visible to you
            for (int i = 0; i < entities; i++) {
                int entityId = in.nextInt(); // buster id or ghost id
                int x = in.nextInt();
                int y = in.nextInt(); // position of this buster / ghost
                int entityType = in.nextInt(); // the team id if it is a buster, -1 if it is a ghost.
                int state = in.nextInt(); // For busters state. For ghost - current stamina.
                int value = in.nextInt(); // For busters: Ghost id being carried. For ghosts: number of busters attempting to trap this ghost.
                
                if(entityType == -1){
                    game.updateGhost(entityId, x, y, state, value);
                }else if(entityType == myTeamId){
                    game.updateBuster(entityId-myTeamId*bustersPerPlayer, x,y, state, value);
                }else{
                    game.updateOpponent(entityId, x, y, state, value);
                }
            }
            System.err.print("CoolGhosts: \n");
            for(Ghost g:game.coolGhosts.values()){
                System.err.print(g.getId());
                System.err.print(" ");
            }
            System.err.print("\n");
            for (int i = 0; i < bustersPerPlayer; i++) {
                System.out.println(game.busterAction(i)); // MOVE x y | BUST id | RELEASE
            }
            game.nextRound();
        }
    }
}
